//
// VARIABLES
//

var player = document.getElementById("player");
var isMoving = false;
var movingCount = 0;
var playerPostionX = 1;
var playerPostionY = 5;
var frame = 0;
var facingRight = true;
var jumping = false;
var reseting = false
var frame = 0
var resetRToLDone = false
var resetLToRDone = false
//
// EVENT LISTENER
//

document.addEventListener('keydown', (event) => {
  const nomTouche = event.key;
  if (nomTouche === 'ArrowLeft' || "ArrowRight") {
    if (!jumping && !isMoving && !reseting) {
      checkReset()
      isMoving = true;
      move(nomTouche, movingCount);
    }
  }
  if (nomTouche === 'ArrowUp') {
    if (!jumping) {
      var j = 30;
      jump(playerPostionY,j);
    }
  }
}, false);

//
// FUNCTIONS
//

function move (nomTouche, movingCount) {
  checkFrame(playerPostionX, frame, nomTouche)
  setTimeout(function () {
    if (isMoving) {
      switch (nomTouche) {
        case "ArrowLeft":
        playerPostionX-=0.5
        if (movingCount < 10) {
          movingCount++
          if (movingCount > 0) {
            player.style.backgroundImage = 'url("./ressources/player-still-left.png")'
            facingRight = false;
          }
        } else {
          movingCount = -10
          player.style.backgroundImage = 'url("./ressources/player-moving-left.png")'
          facingRight = false;
        }
        break;
        case "ArrowRight":
        playerPostionX+=0.5
        if (movingCount < 10) {
          movingCount++
          if (movingCount > 0) {
            player.style.backgroundImage = 'url("./ressources/player-still-right.png")'
            facingRight = true;
          }
        } else {
          movingCount = -10
          player.style.backgroundImage = 'url("./ressources/player-moving-right.png")'
          facingRight = true;
        }
        break;
        default:
      }
      player.style.left = playerPostionX+"%";
      move(nomTouche, movingCount);
      document.addEventListener('keyup', (event) => {
        const nomTouche = event.key;
        if (nomTouche === 'ArrowLeft' || "ArrowRight") {
          return isMoving = false;
        }
      }, false);
    }
    if (!isMoving) {
      switch (nomTouche) {
        case "ArrowRight":
        player.style.backgroundImage = 'url("./ressources/player-still-right.png")'
        facingRight = true;
        break;
        case "ArrowLeft":
        player.style.backgroundImage = 'url("./ressources/player-still-left.png")'
        facingRight = false;
        break;
        default:
      }
    }
  }, 10)
}


function jump(i,j) {
  jumping = true
  setTimeout(function (){
      if (i<30) {
        i++
        player.style.bottom = i+"%"
        if (facingRight) {
          player.style.backgroundImage = 'url("./ressources/player-jumping-right.png")'
        } else {
          player.style.backgroundImage = 'url("./ressources/player-jumping-left.png")'
        }
        jump(i,j)
      } else if (j>5) {
        j--
        player.style.bottom = j+"%"
        if (facingRight) {
          player.style.backgroundImage = 'url("./ressources/player-jumping-right.png")'
        } else {
          player.style.backgroundImage = 'url("./ressources/player-jumping-left.png")'
        }
        jump(i,j)
      }
  }, 0)
  if (j===5 && facingRight) {
    player.style.backgroundImage = 'url("./ressources/player-still-right.png")'
    jumping = false
  } else if (j===5 && !facingRight) {
    player.style.backgroundImage = 'url("./ressources/player-still-left.png")'
    jumping = false
  }
}


function checkFrame(playerPostionX, frame, nomTouche) {
  //frame 0 going left
  if (playerPostionX <= 1 && frame ===0 && nomTouche === 'ArrowLeft') {
    isMoving = false
  }
  //going from frame n to frame n+1
  if (playerPostionX > 92 && nomTouche === 'ArrowRight') {
    isMoving = false
    reseting = true
    resetPositionXLeft(playerPostionX)
  }
  if (playerPostionX < 2 && nomTouche === 'ArrowLeft' && frame >= 1) {
    isMoving = false
    reseting = true
    resetPositionXRight(playerPostionX)
  }
}


function resetPositionXLeft(playerPostionX){
  setTimeout(function () {
    playerPostionX--
    player.style.left = playerPostionX+"%"
    if (playerPostionX >=2) {
      resetPositionXLeft(playerPostionX)
    }else {
      reseting = false
      resetRToLDone = true
    }
  }, 10);
}

function resetPositionXRight(playerPostionX){
  setTimeout(function () {
    playerPostionX++
    player.style.left = playerPostionX+"%"
    if (playerPostionX <=92) {
      resetPositionXRight(playerPostionX)
    }else {
      reseting = false
      resetLToRDone = true
    }
  }, 10);
}

function checkReset(){
  if (resetRToLDone) {
    resetRToLDone = false
    playerPostionX = 2
    frame++
    return
  }
  if (resetLToRDone) {
    resetLToRDone = false
    playerPostionX = 92
    frame--
    return
  }

}
